﻿using System;

namespace FiscalCodeCalculator_Dll
{
    public class FiscalCodeCalculator
    {
        public Person Person { get; set; }
        #region CONSTRUCTORS
        public FiscalCodeCalculator() { }
        public FiscalCodeCalculator(Person person)
        {
            Person = person;
        }
        #endregion
        #region METHODS
        private string CalculateName(string name)
        {
            //Calcolo dei 3 caratteri del nome
            //SE il nome lo consente, le prime 3 consonanti (es: "FEDERICO" = "FDR")
            //SE il nome NON lo consente, tutte le consonanti disponibili e le vocali necessarie (es: "MARA" = "MRA", "IDA" = "DIA")
            //SE il nome è più corto di 3 caratteri, si aggiungo X ("AL" = "LAX")
            return null;
        }
        private string CalculateSurname(string surname)
        {
            //Calcolo dei 3 caratteri del cognome
            //SE il cognome lo consente, le prime 3 consonanti (es: "MORETTI" = "MRT")
            //SE il cognome NON lo consente, tutte le consonanti disponibili e le vocali necessarie (es: "MORO" = "MRO", "ARU" = "RAU")
            //SE il nome è più corto di 3 caratteri, si aggiungo X ("MA" = "MAX")
            return null;
        }
        private string CalculateBirthDay(DateTime birthDay, SEX sex)
        {
            //Calcolo della stringa corrispondente al giorno di nascita
            //ANNO: ultimi due numeri (es: 1970 = 70)
            //MESE: lettera dell'enumeratore ALPHABET corrispondente al carattere corrispodente al numero dell'enumeratore MONTH (es: "Agosto" = 13 = "M")
            //GIORNO: per i maschi giorno di nascita in doppia cifra (es: 13 = 13, 6 = 06), per le femmine giorno di nascita + 40 (es: 13 = 53, 6 = 46)
            return null;
        }
        private string CalculateBirthPlace(string birthPlace, DateTime birthDay)
        {
            //Calcolo della stringa corrispondente al luogo di nascita
            //Codice catastale del comune o dello stato estero di nascita, nell'anno di nascita
            return null;
        }
        private string CalculateControlCode(string content)
        {
            //Calcolo del codice di controllo
            //Si prende la stringa fin qui generata e si converte ogni carattere in un valore numerico
            //I caratteri alfabetici di indice pari corrispondono al valore presente nell'enumeratore CHAR_EVEN
            //I caratteri alfabetici di indice dispari corrispondono al valore presente nell'enumeratore CHAR_ODD
            //I caratteri numerici di indice pari corrispondono al valore presente nell'enumeratore NUMBER_EVEN
            //I caratteri numerici di indice dispari corrispondono al valore presente nell'enumeratore NUMBER_ODD
            //I valori così ottenuti vengono sommati e divisi per 26
            //Il SOLO valore decimale così ottenuto viene confrontato con l'enumeratore CONTROL_CODE
            //La lettera così ottenuta corrisponde al codice di controllo
            return null;
        }
        public string CalculateFiscalCode()
        {
            //COGNOME (3) + NOME (3) + DATA (5) + LUOGO (4) + CODICE DI CONTROLLO (1)
            if (Person.IsValid())
            {
                return null;
            }
            return null;
        }
        #endregion
    }
}
