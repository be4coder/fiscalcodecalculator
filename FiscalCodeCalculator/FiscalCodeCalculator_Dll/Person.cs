﻿using System;

namespace FiscalCodeCalculator_Dll
{
    public class Person
    {
        #region PROPERTIES
        public string Name { get; set; }
        public string Surname { get; set; }
        public string BirthPlace { get; set; }
        public DateTime BirthDay { get; set; }
        public SEX Sex { get; set; }
        #endregion
        #region CONSTRUCTORS
        public Person() { }
        public Person(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }
        public Person(string name, string surname, SEX sex) : this(name, surname)
        {
            Sex = sex;
        }
        #endregion
        #region METHODS
        public bool IsValid()
        {
            return IsNotEmpty(Name) && IsNotEmpty(Surname) && IsNotEmpty(BirthPlace) && BirthDay != null && Sex != SEX.UNDEFINED;
        }
        private bool IsEmpty(string s) => string.IsNullOrEmpty(s) || string.IsNullOrWhiteSpace(s);
        private bool IsNotEmpty(string s) => !IsEmpty(s);
        #endregion
    }
}
