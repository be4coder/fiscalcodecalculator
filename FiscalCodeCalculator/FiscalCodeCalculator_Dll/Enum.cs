﻿namespace FiscalCodeCalculator_Dll
{
    public enum ALPHABETH
    {
        A = 1,
        B = 2, 
        C = 3,
        D = 4,
        E = 5, 
        F = 6, 
        G = 7, 
        H = 8, 
        I = 9,
        J = 10,
        K = 11,
        L = 12, 
        M = 13, 
        N = 14, 
        O = 15, 
        P = 16, 
        Q = 17, 
        R = 18, 
        S = 19, 
        T = 20, 
        U = 21, 
        V = 22,
        W = 23,
        X = 24,
        Y = 25,
        Z = 26
    }
    public enum CHAR_EVEN
    {
        A = 0,
        B = 1,
        C = 2,
        D = 3,
        E = 4,
        F = 5,
        G = 6,
        H = 7,
        I = 8,
        J = 9,
        K = 10,
        L = 11,
        M = 12,
        N = 13,
        O = 14,
        P = 15,
        Q = 16,
        R = 17,
        S = 18,
        T = 19,
        U = 20,
        V = 21,
        W = 22,
        X = 23,
        Y = 24,
        Z = 25
    }
    public enum CHAR_ODD
    {
        A = 1,
        B = 0,
        C = 5,
        D = 7,
        E = 9,
        F = 13,
        G = 15,
        H = 17,
        I = 19,
        J = 21,
        K = 2,
        L = 4,
        M = 18,
        N = 20,
        O = 11,
        P = 3,
        Q = 6,
        R = 8,
        S = 12,
        T = 14,
        U = 16,
        V = 10,
        W = 22,
        X = 25,
        Y = 24,
        Z = 23
    }
    public enum CONTROL_CODE
    {
        A = 0,
        B = 1,
        C = 2,
        D = 3,
        E = 4,
        F = 5,
        G = 6,
        H = 7,
        I = 8,
        J = 9,
        K = 10,
        L = 11,
        M = 12,
        N = 13,
        O = 14,
        P = 15,
        Q = 16,
        R = 17,
        S = 18,
        T = 19,
        U = 20,
        V = 21,
        W = 22,
        X = 23,
        Y = 24,
        Z = 25
    }
    public enum MONTH
    {
        JANUARY = 1,
        FEBRUARY = 2,
        MARCH = 3,
        APRIL = 4,
        MAY = 5,
        JUNE = 8,
        JULY = 12,
        AUGUST = 13,
        SEPTEMBER = 16,
        OCTOBER = 18,
        NOVEMBER = 19,
        DECEMBER = 20
    }
    public enum NUMBER
    {
        ZERO = 0,
        ONE = 1,
        TWO = 2,
        THREE = 3,
        FOUR = 4,
        FIVE = 5,
        SIX = 6,
        SEVEN = 7,
        EIGHT = 8,
        NINE = 9
    }
    public enum NUMBER_EVEN
    {
        ZERO = 0,
        ONE = 1,
        TWO = 2,
        THREE = 3,
        FOUR = 4,
        FIVE = 5,
        SIX = 6,
        SEVEN = 7,
        EIGHT = 8,
        NINE = 9
    }
    public enum NUMBER_ODD
    {
        ZERO = 1,
        ONE = 0,
        TWO = 5,
        THREE = 7,
        FOUR = 9,
        FIVE = 13,
        SIX = 15,
        SEVEN = 17,
        EIGHT = 19,
        NINE = 21
    }
    public enum SEX
    {
        UNDEFINED,
        MALE,
        FEMALE
    }

}
